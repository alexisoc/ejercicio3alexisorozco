package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner input=new Scanner (System.in);
		System.out.println("Da una cadena de texto");
		String cadena=input.nextLine();
		
		System.out.println("Tu cadena de texto: "+cadena);
		System.out.println("Tu cadena de texto en minusculas: "+cadena.toLowerCase());
		
		
		input.close();
	}

}
